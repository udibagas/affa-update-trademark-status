const parseTrademarkData = (data) => {
  const {
    nomor_permohonan,
    nama_merek,
    status_permohonan,
    tanggal_permohonan,
    nomor_pengumuman,
    tanggal_pengumuman,
    tanggal_dimulai_perlindungan,
    translasi,
    owner,
    consultant,
    t_class,
  } = data;

  const { tm_owner_name, tm_owner_address, country_code, country_name } =
    owner[0];

  const {
    reprs_name,
    reprs_address,
    country_id: reprs_country_id,
    country_name: reprs_country_name,
  } = consultant.length
    ? consultant[0]
    : { reprs_name: "", reprs_address: "", country_id: "", country_name: "" };

  const class_no = t_class[0].class_no;
  const class_desc = t_class.map((c) => c.class_desc).join(",\n");

  // order matters
  return {
    nomor_permohonan,
    nama_merek,
    status_permohonan,
    tanggal_permohonan,
    nomor_pengumuman,
    tanggal_pengumuman,
    tanggal_dimulai_perlindungan,
    translasi,
    tm_owner_name,
    tm_owner_address,
    country_code,
    country_name,
    reprs_name,
    reprs_address,
    reprs_country_id,
    reprs_country_name,
    class_no,
    class_desc,
  };
};

const parsePatentData = (data) => {
  const {
    id: nomor_permohonan,
    nomor_sertifikat,
    // tanggal_pemberian,
    status_permohonan,
    judul_permohonan,
    nomor_publikasi,
    tanggal_publikasi,
    tanggal_dimulai_perlindungan,
    tanggal_penerimaan,
    tanggal_berakhir_perlindungan,
    nama_pemeriksa,
    abstract,
    inventor,
    owner,
    ipc,
    priority,
    reps,
  } = data;

  const nama_inventor = inventor.map((i) => i.nama_inventor).join(";");
  const alamat_inventor = inventor.map((i) => i.alamat_inventor).join(";");
  const negara_inventor = inventor.map((i) => i.nationality).join(";");

  const nama_pemegang = owner.map((i) => i.nama_pemegang).join(";");
  const alamat_pemegang = owner.map((i) => i.alamat_pemegang).join(";");
  const negara_pemegang = owner.map((i) => i.nationality).join(";");

  const ipc_list = ipc.map((i) => i.ipc).join(";");

  const nomor_prioritas = priority.map((i) => i.nomor_prioritas).join(";");
  const tanggal_prioritas = priority.map((i) => i.tanggal).join(";");
  const negara_prioritas = priority.map((i) => i.nationality).join(";");

  const nama_konsultan = reps.map((i) => i.nama_konsultan).join(";");
  const alamat_konsultan = reps.map((i) => i.alamat_konsultan).join(";");
  const negara_konsultan = reps.map((i) => i.nationality).join(";");

  // order matters
  return {
    nomor_permohonan,
    nomor_sertifikat,
    status_permohonan,
    judul_permohonan,
    nomor_publikasi,
    tanggal_publikasi,
    tanggal_penerimaan,
    tanggal_dimulai_perlindungan,
    tanggal_berakhir_perlindungan,
    nama_pemeriksa,
    nomor_prioritas,
    tanggal_prioritas,
    negara_prioritas,
    ipc_list,
    nama_pemegang,
    alamat_pemegang,
    negara_pemegang,
    nama_inventor,
    alamat_inventor,
    negara_inventor,
    nama_konsultan,
    alamat_konsultan,
    negara_konsultan,
    abstract,
  };
};

module.exports = { parseTrademarkData, parsePatentData };
