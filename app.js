const fs = require("fs");
const jwt = require("jsonwebtoken");
const { google } = require("googleapis");
const fetch = require("cross-fetch");
const sendMail = require("./services/mailer.service");
const {
  parseTrademarkData,
  parsePatentData,
} = require("./services/data-parser.service");

const apiUrl = {
  // trademark: "https:pdki-indonesia-api.dgip.go.id/api/trademark/detail/", // kalau pakai ini id & nomor_permohonan sama
  trademark: "https://pdki-indonesia-api.dgip.go.id/api/trademark/search",
  patent: "https://pdki-indonesia-api.dgip.go.id/api/patent/",
};

const spreadsheetId = {
  trademark: "1xP9a6rFFQE2Ku7gVg5ruBh0T4i22XTR-Sf8EPVkGZ2Y",
  patent: "1epHZQRNZ0_A75CYWVpJfhgPvzD5Zpzqv4Vl6_pcXncQ",
};

const auth = new google.auth.GoogleAuth({
  keyFile: "credentials.json",
  scopes: "https://www.googleapis.com/auth/spreadsheets",
});

const sheets = [
  "emirsyah",
  "farizsyah",
  "adastra",
  "avia",
  "2016",
  "2017",
  "2018",
  "2019",
  "2020",
  "2021",
  "2022",
  "2023",
];

const checkStatus = ["(TM) Didaftar", "(PA) Diberi Paten"];
const skippedStatus = [
  "(TM) Ditolak",
  "(TM) Ditolak Berdasarkkan Tanggapan",
  "(TM) Ditolak KBM",
];

const check = async (type, sheet) => {
  console.log(`Checking sheet ${sheet} - ${type}...`);
  // Create client instance for auth
  const client = await auth.getClient();
  // Instance of Google Sheets API
  const googleSheets = google.sheets({ version: "v4", auth: client });

  // Read rows from spreadsheet
  try {
    const getRows = await googleSheets.spreadsheets.values.get({
      auth,
      spreadsheetId: spreadsheetId[type],
      range: `${sheet}!A:R`,
    });

    const rows = getRows.data.values.slice(2); // 2 first rows are headers
    const newValues = [];

    //! Jangan pakai forEach karena dia async
    for (let index = 0; index < rows.length; index++) {
      const row = rows[index];
      const [nomor_permohonan, , oldStatus] = row;
      const data = await getStatusUpdate(type, nomor_permohonan, oldStatus);

      if (!data) {
        console.log(`${nomor_permohonan} - No data`);
        newValues.push(row); // timpa lagi dengan data lama
      } else {
        console.log(`${nomor_permohonan} - Updated`);
        newValues.push(Object.values(data));
        const { status_permohonan } = data;
        if (checkStatus.includes(status_permohonan)) {
          sendNotification(type, data);
        }
      }
    }

    const request = {
      auth,
      spreadsheetId: spreadsheetId[type],
      requestBody: {
        valueInputOption: "USER_ENTERED",
        data: newValues.map((row, i) => {
          const rowNumber = i + 3;
          const range = `${sheet}!A${rowNumber}:R${rowNumber}`;
          return {
            range,
            majorDimension: "ROWS",
            values: [row],
          };
        }),
      },
    };

    await googleSheets.spreadsheets.values.batchUpdate(request);
    console.log(`Sheets ${sheet} has been updated!`);
  } catch (error) {
    console.log(error.message);
  }
};

const getStatusUpdate = async (type, nomor_permohonan, oldStatus) => {
  if ([...checkStatus, ...skippedStatus].includes(oldStatus)) {
    return null;
  }

  try {
    const Session = jwt.sign({ foo: 71 }, "rahasia", { expiresIn: "1d" });

    let headersList = {
      Accept: "*/*",
      "User-Agent": "Thunder Client (https://www.thunderclient.com)",
      "Content-Type": "application/json",
      Session,
    };

    let bodyContent = JSON.stringify({
      key: "9fc68abdda163a3460671bd68e3e760521045d6984cd5a98922874563e9b6851",
      keyword: nomor_permohonan,
      page: 1,
      showFilter: true,
      type,
      order_state: "asc",
    });

    let res = await fetch(
      type == "trademark" ? apiUrl[type] : apiUrl[type] + nomor_permohonan,
      {
        method: "POST",
        body: bodyContent,
        headers: headersList,
      }
    );

    if (!res.ok) return null;

    if (type == "trademark") {
      const { hits, data } = await res.json();
      if (hits?.hits?.length == 0) return null;
      return parseTrademarkData(hits.hits[0]._source);
      // if (!data) return null;
      // return parseTrademarkData(data);
    }

    if (type == "patent") {
      const { data } = await res.json();
      if (!data) return null;
      return parsePatentData(data);
    }

    return null;
  } catch (error) {
    console.log(error.message);
    return false;
  }
};

const sendNotification = (type, data) => {
  const recipient = {
    trademark: "statusmerek@affa.co.id",
    patent: "statuspatent@affa.co.id",
  };

  const { nomor_permohonan, status_permohonan } = data;
  let html = fs.readFileSync(`./${type}-notification.html`, "utf-8");

  // parsing template
  Object.keys(data).forEach((k) => {
    html = html.replaceAll(`{${k}}`, data[k]);
  });

  sendMail(
    `Status nomor ${nomor_permohonan} ${status_permohonan}`,
    recipient[type],
    html
  );
};

Object.keys(spreadsheetId).forEach((type) => {
  sheets.forEach(async (sheet) => {
    await check(type, sheet);
  });
});
